jQuery(document).ready(function (jQuery) {
    /**
     * the actual form submission
     * gets the input and send it off an ajax call
     */
    jQuery(".element_modal_form").submit( function( e ) {
        e.preventDefault();

        var security = jQuery(this).find("#security").val();
        var id = jQuery(this).find('input[id="id"]').val();
        var name = jQuery(this).find('input[id="name"]').val();
        var add_edit = "add";

        if ( id > 0 ) {
            add_edit = "edit";
        } else {
            add_edit = "add";
        }

        var data = {
            action: 'add_edit_element',
            add_edit: add_edit,
            security: security,
            id: id,
            name: name,
        }

        //  the good ole Ajax and response
        jQuery.post(ajaxurl, data, function(response) {
            var alertMsg = add_edit == "edit" ? 'updated' : 'added';

            if (!alert("The element has been " + alertMsg + ".")) {
                window.location.reload();
            }
        });
    });

    /**
     * the load unit from the display table
     */
    jQuery('.load_element').submit( function ( e ) {
        e.preventDefault();

        var security = jQuery(this).find("#security").val();
        var id = jQuery(this).find( 'input[id="id"]').val();

        var data = {
            action: 'load_element',
            id: id,
            security: security,
        }

        jQuery.post(ajaxurl, data, function(response) {
            var element = jQuery.parseJSON(response);
            jQuery('.element_unit_form').find('input[id="id"]').val( element.id );
            jQuery('.element_unit_form').find('input[id="name"]').val( element.name );

            var url = "#TB_inline?width=600&height=550&inlineId=element";
            tb_show("Edit Element", url);
        });
    });

    jQuery(".delete_element").submit(function(e) {
        e.preventDefault();

        var security = jQuery(this).find("#security").val();
        var id = jQuery(this).find('input[id="id"]').val();

        var data = {
            action: 'delete_element',
            security: security,
            id: id
        }

        if (confirm("Are you sure you want to delete this element?")) {
            jQuery.post( ajaxurl, data, function(response) {
                if (!alert("The element has been deleted.")) {
                    window.location.reload();
                }
            });
        }
    });

    // Setup - add a text input to each footer cell
    jQuery('#element_display_table tfoot th').each( function () {
        var title = jQuery(this).text();
        jQuery(this).html( '<input type="text" placeholder="Search" />' );
    } );

    var table = jQuery("#element_display_table").DataTable({
        "order": [[ 1, "asc" ]]
    });  //  make all our output tables a data table
});