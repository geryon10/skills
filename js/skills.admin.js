jQuery(document).ready(function (jQuery) {
    jQuery('#add_new_skill').on('click', function(){
        jQuery('.skill_modal_form').find('input[id="id"]').val('');
        jQuery('.skill_modal_form').find('input[id="name"]').val('');
        jQuery('.skill_modal_form').find('textarea[id="description"]').val('');
        jQuery('.skill_modal_form').find('select[id="elements"]').val('').change();
        jQuery('.skill_modal_form').find('select[id="races"]').val('').change();
        jQuery('.skill_modal_form').find('select[id="skill_type"]').val('').change();
        jQuery('#skill_pre_req_list').html('');
        jQuery('.skill_modal_form').find('input[id="is_one_shot"]').val('0');
        jQuery('.skill_modal_form').find('input[id="is_one_shot"]').prop('checked', false);
        jQuery('.skill_modal_form').find('input[id="is_secret_skill"]').val('0');
        jQuery('.skill_modal_form').find('input[id="is_secret_skill"]').prop('checked', false);
    });
    /**
     * the actual form submission
     * gets the input and send it off an ajax call
     */
    jQuery(".skill_modal_form").submit(function(e) {
        e.preventDefault();

        var security = jQuery(this).find("#security").val();
        var id = jQuery(this).find('input[id="id"]').val();
        var add_edit = id > 0 ? 'edit' : 'add';
        var name = jQuery(this).find('input[id="name"]').val();
        var description = jQuery(this).find('textarea[id="description"]').val();
        var elements = jQuery(this).find('select[id="elements"]').val();
        var race_id = jQuery(this).find('select[id="races"]').val();
        var type = jQuery(this).find('select[id="skill_type"]').val();
        var pre_req = jQuery(this).find('input[name="pre_req[]"]').map(function(){return jQuery(this).val();}).get();
        var is_one_shot = jQuery( 'input[id="is_one_shot"]' ).prop('checked') ? 1 : 0;
        var is_secret = jQuery( 'input[id="is_secret_skill"]' ).prop('checked') ? 1 : 0;

        if ( id > 0 ) {
            add_edit = "edit";
        } else {
            add_edit = "add";
        }

        var data = {
            action: 'add_edit_skill',
            add_edit: add_edit,
            security: security,
            id: id,
            name: name,
            description: description,
            is_one_shot: is_one_shot,
            elements: elements,
            race_id: race_id,
            pre_req: pre_req,
            is_secret: is_secret,
            type: type,
        }

        //  the good ole Ajax and response
        jQuery.post(ajaxurl, data, function(response) {
            var alertMsg = add_edit == "edit" ? 'updated' : 'added';

            if (!alert("The skill has been " + alertMsg + ".")) {
                window.location.reload();
            }
        });
    });

    /**
     * the load unit from the display table
     */
    jQuery('.load_skill').submit(function (e) {
        e.preventDefault();

        var security = jQuery(this).find("#security").val();
        var id = jQuery(this).find( 'input[id="id"]').val();

        var data = {
            action: 'load_skill',
            id: id,
            security: security,
        }

        jQuery.post(ajaxurl, data, function(response) {
            var skill = jQuery.parseJSON(response);

            jQuery('.skill_modal_form').find('input[id="id"]').val(skill.id);
            jQuery('.skill_modal_form').find('input[id="name"]').val(skill.name);
            jQuery('.skill_modal_form').find('textarea[id="description"]').val(skill.description);
            jQuery('.skill_modal_form').find('select[id="elements"]').val('').change();
            jQuery('.skill_modal_form').find('select[id="races"]').val('').change();
            jQuery('.skill_modal_form').find('select[id="skill_type"]').val('').change();
            jQuery('#skill_pre_req_list').html('');
            jQuery('.skill_modal_form').find('input[id="is_one_shot"]').val('0');
            jQuery('.skill_modal_form').find('input[id="is_one_shot"]').prop('checked', false);
            jQuery('.skill_modal_form').find('input[id="is_secret_skill"]').val('0');
            jQuery('.skill_modal_form').find('input[id="is_secret_skill"]').prop('checked', false);

            var elements = [];
            //  add each element to the element select
            for (var i = 0; i < skill.elements.length; i++) {
                elements.push(skill.elements[i]);
            }
            jQuery('.element_multiple').select2({
                dropdownParent: jQuery('.carps_modal_form')
            });

            if (skill.isOneShot == 1) {
                jQuery('.skill_modal_form').find('input[id="is_one_shot"]').val('1');
                jQuery('.skill_modal_form').find('input[id="is_one_shot"]').prop('checked', true);
            } else {
                jQuery('.skill_modal_form').find('input[id="is_one_shot"]').val('0');
                jQuery('.skill_modal_form').find('input[id="is_one_shot"]').prop('checked', false);
            }

            if (skill.isSecret == 1) {
                jQuery('.skill_modal_form').find('input[id="is_secret_skill"]').val('1');
                jQuery('.skill_modal_form').find('input[id="is_secret_skill"]').prop('checked', true);
            } else {
                jQuery('.skill_modal_form').find('input[id="is_secret_skill"]').val('0');
                jQuery('.skill_modal_form').find('input[id="is_secret_skill"]').prop('checked', false);
            }

            jQuery('.skill_modal_form').find('select[id="elements"]').val(elements).change();
            jQuery('.skill_modal_form').find('select[id="races"]').val(skill.race_id).change();
            jQuery('.skill_modal_form').find('select[id="skill_type"]').val(skill.type).change();

            //  add the prerequisites
            // console.log(js_skills);
            for (i = 0; i < skill.preReqs.length; i++) {
                for (var key in skill.preReqs[i]) {
                    if (key > 0) {
                        pre_req = key;
                        pre_req_name = js_skills[key];
                        pre_req_lvl = skill.preReqs[i][key];
                        random = getRandomIntInclusive(1, 999999);

                        remove = generatePreReqRemove(random);
                        display = generatePreReqDisplay(pre_req_name, pre_req_lvl);
                        li = generatePreReqLi(random, display, remove);
                        input = generatePreReqInput(random, pre_req, pre_req_lvl);

                        jQuery("#skill_pre_req_list").append(li);
                        jQuery("#hidden_pre_req_list").append(input);
                    }
                }
            }

            var url = "#TB_inline?width=600&height=550&inlineId=skill_modal";
            tb_show("Edit Skill", url);
        });
    });

    jQuery('#add_pre_req').click(function(e) {
        e.preventDefault();

        var random = getRandomIntInclusive(1, 999999);

        var pre_req = jQuery('#pre_req').val();
        var pre_req_name = jQuery('#pre_req option:selected').text();
        var pre_req_lvl = jQuery('#pre_req_lvl').val();

        var remove = generatePreReqRemove(random);
        var display = generatePreReqDisplay(pre_req_name, pre_req_lvl);
        var li = generatePreReqLi(random, display, remove);
        var input = generatePreReqInput(random, pre_req, pre_req_lvl);

        jQuery("#skill_pre_req_list").append(li);
        jQuery("#hidden_pre_req_list").append(input);
    });

    jQuery('.skill_modal_form').on("click", '.remove_pre_req', function(e) {
        e.preventDefault();

        var id = jQuery(this).attr('id');

        var li = document.getElementById('pre_req_display_' + id);
        var input = document.getElementById('pre_req_' + id);

        li.remove();
        input.remove();
    });

    // Setup - add a text input to each footer cell
    jQuery('#skill_display_table tfoot th').each(function() {
        jQuery(this).text();
        jQuery(this).html('<input type="text" placeholder="Search" />');
    });

    jQuery("#skill_display_table").DataTable({
        "order": [[1, "asc"]]
    });  //  make all our output tables a data table

    jQuery('.element_multiple').select2({
        dropdownParent: jQuery('.carps_modal_form'),
        // sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
        sorter: function(data) {
            return data.sort();
        }
    });

    jQuery('.pre_req').select2({
        dropdownParent: jQuery('.carps_modal_form'),
        sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
    });

    jQuery('.races').select2({
        dropdownParent: jQuery('.carps_modal_form'),
        sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
    });

    jQuery('.skill_type').select2({
        dropdownParent: jQuery('.carps_modal_form'),
        sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
    });
});

function deleteSkill(id, security, name)
{
    var data = {
        action: 'delete_skill',
        security: security,
        id: id
    }

    if (confirm("Are you sure you want to delete " + name + "?")) {
        jQuery.post(ajaxurl, data, function(response) {
            if (!alert("The skill has been deleted.")) {
                window.location.reload();
            }
        });
    }
}

function generatePreReqRemove(random)
{
    return '<button class="button remove_pre_req" type="button" id="' + random + '">X</button>';
}

function generatePreReqDisplay(pre_req_name, pre_req_lvl)
{
    return '<div>' + pre_req_name + ' lvl ' + pre_req_lvl + '</div>';
}

function generatePreReqLi(random, display, remove)
{
    return '<li class="pre_req_display" id="pre_req_display_' + random + '">' + display + ' ' + remove + '</li>';
}

function generatePreReqInput(random, pre_req, pre_req_lvl)
{
    return '<input id="pre_req_' + random + '" type="hidden" name="pre_req[]" value="' + pre_req + ":" + pre_req_lvl + '" />';
}