jQuery(document).ready(function (jQuery) {
    jQuery('#add_new_race').on('click', function(){
        jQuery('.race_modal_form').find('input[id="id"]').val('');
        jQuery('.race_modal_form').find('input[id="name"]').val('');
        jQuery('.race_modal_form').find('textarea[id="description"]').val('');
        jQuery('.race_modal_form').find('input[id="is_secret_race"]').val('0');
        jQuery('.race_modal_form').find('input[id="is_secret_race"]').prop('checked', false);
    });
    /**
     * the actual form submission
     * gets the input and send it off an ajax call
     */
    jQuery(".race_modal_form").submit( function( e ) {
        e.preventDefault();

        var security = jQuery(this).find("#security").val();
        var id = jQuery(this).find('input[id="id"]').val();
        var name = jQuery(this).find('input[id="name"]').val();
        var description = jQuery(this).find('textarea[id="description"]').val();
        var is_secret = jQuery( 'input[id="is_secret_race"]' ).prop('checked') ? 1 : 0;
        var add_edit = "add";

        if ( id > 0 ) {
            add_edit = "edit";
        } else {
            add_edit = "add";
        }

        var data = {
            action: 'add_edit_race',
            add_edit: add_edit,
            security: security,
            id: id,
            name: name,
            description: description,
            is_secret: is_secret,
        }

        console.log(data);

        //  the good ole Ajax and response
        jQuery.post(ajaxurl, data, function(response) {
            var alertMsg = add_edit == "edit" ? 'updated' : 'added';

            if (!alert("The race has been " + alertMsg + ".")) {
                window.location.reload();
            }
        });
    });

    /**
     * the load unit from the display table
     */
    jQuery('.load_race').submit( function ( e ) {
        e.preventDefault();

        var security = jQuery(this).find("#security").val();
        var id = jQuery(this).find( 'input[id="id"]').val();

        var data = {
            action: 'load_race',
            id: id,
            security: security,
        }

        jQuery.post(ajaxurl, data, function(response) {
            var race = jQuery.parseJSON(response);

            jQuery('.race_modal_form').find('input[id="id"]').val(race.id);
            jQuery('.race_modal_form').find('input[id="name"]').val(race.name);
            jQuery('.race_modal_form').find('textarea[id="description"]').val(race.description);
            jQuery('.race_modal_form').find('input[id="is_secret_race"]').val('0');
            jQuery('.race_modal_form').find('input[id="is_secret_race"]').prop('checked', false);

            if (race.isSecret == 1) {
                jQuery('.race_modal_form').find('input[id="is_secret_race"]').val('1');
                jQuery('.race_modal_form').find('input[id="is_secret_race"]').prop('checked', true);
            } else {
                jQuery('.race_modal_form').find('input[id="is_secret_race"]').val('0');
                jQuery('.race_modal_form').find('input[id="is_secret_race"]').prop('checked', false);
            }

            var url = "#TB_inline?width=600&height=550&inlineId=race";
            tb_show("Edit Race", url);
        });
    });

    jQuery(".delete_race").submit(function(e) {
        e.preventDefault();

        var security = jQuery(this).find("#security").val();
        var id = jQuery(this).find('input[id="id"]').val();

        var data = {
            action: 'delete_race',
            security: security,
            id: id
        }

        if (confirm("Are you sure you want to delete this race?")) {
            jQuery.post( ajaxurl, data, function(response) {
                if (!alert("The race has been deleted.")) {
                    window.location.reload();
                }
            });
        }
    });

    // Setup - add a text input to each footer cell
    jQuery('#race_display_table tfoot th').each( function () {
        var title = jQuery(this).text();
        jQuery(this).html( '<input type="text" placeholder="Search" />' );
    } );

    var table = jQuery("#race_display_table").DataTable({
        "order": [[ 1, "asc" ]]
    });  //  make all our output tables a data table
});