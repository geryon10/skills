<?php

/**
 * Plugin Name: CARPS character
 * Description: for tracking characters
 * Version: 1.0
 * Author: Scott Schwab
 * License MIT
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: list
 */

namespace CarpsCharacterBuilder;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Define the base file for enqueuing js files
 * define the base folder for enqueuing php files
 */
if (!defined('CARPS_CHARACTER_FOLDER')) {
    /*
    For enqueueing files into WP
    Use:: wp_register_script( 'inpsyde_admin_main', plugins_url( 'js/admin.main.js', CARPS_CHARACTER_FILE ),
        array( 'jquery', 'jquery-ui-core', 'jquery-ui-sortable' ) );
    */
    define('CARPS_CHARACTER_FILE', __FILE__);

    /*
    For displaying files to the front end
    Use:: $sampleFile = CARPS_CHARACTER_URL . '/files/generic.csv';
    */
    define('CARPS_CHARACTER_URL', plugins_url('', __FILE__));

    /*
    For loading files from PHP
    use:: include_once CARPS_CHARACTER_FOLDER . "/includes/class/generic_class/controller/table.controller.php";
    */
    define('CARPS_CHARACTER_FOLDER', dirname(CARPS_CHARACTER_FILE));

    /*
    for loading files from PHP in the includes/class folder
    */
    define('CARPS_CHARACTER_CLASS_FOLDER', CARPS_CHARACTER_FOLDER . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class');
}

// The object that starts everything!
if (!class_exists('CarpsCharacterBuilder')) {
    require_once CARPS_CHARACTER_FOLDER . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'carpsBuilder.php';
}

/**
 * Runs the object
 */
function run_carps_character_builder()
{
    $plugin = new CarpsCharacterBuilder();
}
run_carps_character_builder();
