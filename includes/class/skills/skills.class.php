<?php

namespace CarpsCharacterBuilder;

class Skills extends CarpsBaseClass
{
    public $elements = array();
    public $eleBridgeAs = '';
    public $preReqs = array();
    public $reqBridgeAs = '';
    public $isOneShot = 0;
    public $race_id = null;
    public $isSecret = 0;
    public $type;
    public $skill_types = array(
        'A' => 'Avoidance',
        'R' => 'Resist'
    );
    public $oneShots = array();

    public function __construct($result = array())
    {
        if (isset($result->id)) {
            $this->setId($result->id);
        }
        if (isset($result->name)) {
            $this->setName($result->name);
        }
        if (isset($result->description)) {
            $this->setDescription($result->description);
        }
        if (isset($result->element_id)) {
            $this->addElement($result->element_id);
        }
        if (isset($result->pre_req_skill_id)) {
            $this->addPreReq($result->pre_req_skill_id, $result->level_required);
        }
        if (isset($result->one_shot_id)) {
            $this->addOneShot($result->one_shot_id, $result->one_shot_level);
        }
        if (isset($result->is_one_shot)) {
            $this->setIsOneShot($result->is_one_shot);
        }
        if (isset($result->race_id)) {
            $this->setRaceId($result->race_id);
        }
        if (isset($result->is_secret)) {
            $this->setIsSecret($result->is_secret);
        }
        if (isset($result->type)) {
            $this->setType($result->type);
        }
        $this->setReqBridgeAs();
        $this->setEleBridge();
        $this->setTableAs($this->db . 'skills');
        $this->base_query = "SELECT `{$this->tableAs}`.`id`,
        `{$this->tableAs}`.`name`,
        `{$this->tableAs}`.`description`,
        `{$this->tableAs}`.`is_one_shot`,
        `{$this->tableAs}`.`race_id`,
        `{$this->tableAs}`.`is_secret`,
        `{$this->tableAs}`.`type`
        FROM `{$this->tableAs}` AS `{$this->tableAs}`
        ";
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function addElement($element_id)
    {
        $this->elements[] = $element_id;
    }

    public function addPreReq($pre_req_skill_id, $level_required)
    {
        $entry = array($pre_req_skill_id => $level_required);
        if (in_array($entry, $this->preReqs) == false) {
            $this->preReqs[] = $entry;
        }
    }

    public function addOneShot($one_shot_id, $one_shot_level)
    {
        $entry = array($one_shot_id => $one_shot_level);
        if (in_array($entry, $this->oneShots) == false) {
            $this->oneShots[] = $entry;
        }
    }

    public function setIsOneShot($is_one_shot)
    {
        $this->isOneShot = $is_one_shot;
    }

    public function setRaceId($race_id)
    {
        $this->raceId = $race_id;
    }

    public function setIsSecret($is_secret)
    {
        $this->isSecret = $is_secret;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setReqBridgeAs()
    {
        $this->reqBridgeAs = 'carps_skill_pre_req_bridge';
    }

    public function setEleBridge()
    {
        $this->eleBridgeAs = 'carps_skill_element_bridge';
    }


    public function getElements(): array
    {
        return $this->elements;
    }

    public function getPreReqs(): array
    {
        return $this->preReqs;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getIsOneShot(): int
    {
        return $this->isOneShot;
    }

    public function getRaceId()
    {
        return $this->raceId;
    }

    public function getIsSecret(): int
    {
        return $this->isSecret;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getReqBridgeAs()
    {
        return $this->reqBridgeAs;
    }

    public function getEleBridgeAs()
    {
        return $this->eleBridgeAs;
    }


    public function getAllFromDb($where = null): array
    {
        global $wpdb;

        $query = $this->getSkillQuery();

        if ($where != null) {
            $query .= ' ' . $where;
        }

        $results = $wpdb->get_results($wpdb->prepare($query));

        $return_array = array();
        if (count($results) > 0) {
            foreach ($results as $result) {
                $return_array = $this->organizeSkill($result, $return_array);
            }
        }

        return $return_array;
    }

    public function getFromDbById($id): array
    {
        global $wpdb;

        $query = $this->getSkillQuery();
        $query .= " WHERE `{$this->tableAs}`.`id` = '%d'";

        $results = $wpdb->get_results($wpdb->prepare($query, $id));

        $return_array = array();
        if (count($results) > 0) {
            foreach ($results as $result) {
                $return_array = $this->organizeSkill($result, $return_array);
            }
            $skill[0] = $return_array[$result->id];
        }

        return $skill;
    }

    private function getSkillQuery(): string
    {
        $query = "SELECT `{$this->tableAs}`.`id`,
        `{$this->tableAs}`.`name`,
        `{$this->tableAs}`.`description`,
        `{$this->tableAs}`.`is_one_shot`,
        `{$this->tableAs}`.`race_id`,
        `{$this->tableAs}`.`is_secret`,
        `{$this->tableAs}`.`type`,
        `{$this->eleBridgeAs}`.`element_id`,
        `{$this->reqBridgeAs}`.`pre_req_skill_id`,
        `{$this->reqBridgeAs}`.`level_required`,
        `one_shots`.`id` AS `one_shot_id`,
        `one_shots`.`name` AS `one_shot_name`,
        `one_shots`.`description` AS `one_shot_description`,
        `one_shots`.`is_one_shot` AS `one_shot_is_one_shot`,
        `one_shots`.`race_id` AS `one_shot_race_id`,
        `one_shots`.`is_secret` AS `one_shot_is_secret`,
        `one_shots`.`type` AS `one_shot_type`,
        `one_shots_bridge`.`level_required` AS `one_shot_level`
        FROM `{$this->tableAs}` AS `{$this->tableAs}`
        LEFT JOIN `{$this->eleBridgeAs}` AS `{$this->eleBridgeAs}` ON `{$this->eleBridgeAs}`.`skill_id` = `{$this->tableAs}`.`id`
        LEFT JOIN `{$this->reqBridgeAs}` AS `{$this->reqBridgeAs}` ON `{$this->reqBridgeAs}`.`skill_id` = `{$this->tableAs}`.`id`
        LEFT JOIN `{$this->reqBridgeAs}` AS `one_shots_bridge` ON `{$this->tableAs}`.`id` = `one_shots_bridge`.`pre_req_skill_id`
        LEFT JOIN `{$this->tableAs}` AS `one_shots` ON `one_shots`.`id` = `one_shots_bridge`.`skill_id` AND `one_shots`.`is_one_shot` = 1
        ";

        return $query;
    }

    private function organizeSkill($result, $return_array)
    {
        if (array_key_exists($result->id, $return_array) == true) {
            //  check if the element is already in the array
            if (
                $result->element_id > 0
                && \in_array($result->element_id, $return_array[$result->id]->elements) == false
            ) {
                $return_array[$result->id]->addElement($result->element_id);
            }
            //  check if the prerequisite is already in the array
            if (
                $result->pre_req_skill_id > 0
                && \in_array($result->pre_req_skill_id, $return_array[$result->id]->preReqs) == false
            ) {
                $return_array[$result->id]->addPreReq($result->pre_req_skill_id, $result->level_required);
            }
            //  check if the one shot is already in the array
            if (
                $result->one_shot_id > 0
                && \in_array($result->one_shot_id, $return_array[$result->id]->oneShots) == false
            ) {
                $return_array[$result->id]->addOneShot($result->one_shot_id, $result->one_shot_level);
            }
        } else {
            $return_array[$result->id] = new Skills($result);
        }
        return $return_array;
    }

    public function createObject()
    {
        $object = new \stdClass();
        $object->id = $this->getId();
        $object->name = $this->getName();
        $object->description = $this->getDescription();
        $object->isOneShot = $this->getIsOneShot();
        $object->race_id = $this->getRaceId();
        $object->isSecret = $this->getIsSecret();
        $object->type = $this->getType();
        $object->elements = $this->getElements();
        $object->preReqs = $this->getPreReqs();

        return $object;
    }

    public function ajaxAddEditSkill()
    {
        global $wpdb;

        check_ajax_referer('skill_form', 'security');

        $id = $_POST['id'];
        $add_edit = sanitize_text_field($_POST['add_edit']);
        $name = sanitize_text_field($_POST['name']);
        $description = sanitize_textarea_field($_POST['description']);
        $is_one_shot = sanitize_text_field($_POST['is_one_shot']);
        $race_id = sanitize_text_field($_POST['race_id']);
        $is_secret = sanitize_text_field($_POST['is_secret']);
        $type = sanitize_text_field($_POST['type']);

        if (is_array($_POST['elements']) == true) {
            $elements = array_map('esc_attr', $_POST['elements']);
        } else {
            $elements = array();
        }
        if (is_array($_POST['pre_req']) == true) {
            $pre_req = array_map('esc_attr', $_POST['pre_req']);
        } else {
            $pre_req = array();
        }

        if ($add_edit === 'add') {
            $created = date("Y-m-d H:i:s");
            $wpdb->insert(
                $this->getTableAs(),
                array(
                    'name'        => $name,
                    'description' => $description,
                    'is_one_shot' => $is_one_shot,
                    'race_id'     => $race_id,
                    'is_secret'   => $is_secret,
                    'type'        => $type,
                ),
                array(
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                )
            );

            $id = $wpdb->insert_id;
        } elseif ($add_edit === 'edit') {
            $wpdb->update(
                $this->getTableAs(),
                array(
                    'name'        => $name,
                    'description' => $description,
                    'is_one_shot' => $is_one_shot,
                    'race_id'     => $race_id,
                    'is_secret'   => $is_secret,
                    'type'        => $type,
                ),
                array( 'id' => $id ),
                array(
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                ),
                array('%d')
            );

            //  delete the old elemental bridge so we can just write new ones latter
            $wpdb->delete(
                $this->getEleBridgeAs(),
                array('skill_id'   => $id,),
                array('%d',)
            );
            //  delete the old prerequisite bridges

            $wpdb->delete(
                $this->getReqBridgeAs(),
                array('skill_id'   => $id,),
                array('%d',)
            );
        } else {
            echo "unknown action";
        }

        //  add the elemental bridges
        if ($id > 0 && count($elements) > 0) {
            foreach ($elements as $element_id) {
                $wpdb->insert(
                    $this->getEleBridgeAs(),
                    array(
                        'element_id' => $element_id,
                        'skill_id'   => $id,
                    ),
                    array(
                        '%d',
                        '%d',
                    )
                );
            }
        }

        //  add the pre prerequisite bridges
        if ($id > 0 && count($pre_req) > 0) {
            foreach ($pre_req as $req) {
                $req_array = \explode(':', $req);
                $req_id = $req_array[0];
                $level = $req_array[1];

                $wpdb->insert(
                    $this->getReqBridgeAs(),
                    array(
                        'skill_id'         => $id,
                        'pre_req_skill_id' => $req_id,
                        'level_required'   => $level
                    ),
                    array(
                        '%d',
                        '%d',
                        '%d',
                    )
                );
            }
        }
        wp_die();
    }

    public function ajaxDeleteSkill()
    {
        $id = sanitize_text_field($_POST['id']);

        global $wpdb;

        $wpdb->delete(
            'carps_skill_element_bridge',
            array('skill_id'   => $id,),
            array('%d',)
        );

        $wpdb->delete(
            $this->$reqBridgeAs,
            array('skill_id'   => $id,),
            array('%d',)
        );

        $wpdb->delete(
            $this->tableAs,
            array('id' => $id),
            array('%d')
        );
        wp_die();
    }

    public function getOneShots(): array
    {
        $oneShots = array();
        $where = " WHERE `{$this->tableAs}`.`is_one_shot` = 1
        AND `{$this->reqBridgeAs}`.`pre_req_skill_id` =  " . $this->getId();

        $results = $this->getAllFromDb($where);

        return $oneShots;
    }
}
