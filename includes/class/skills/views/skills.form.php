<div id="skill_modal" class="carps_modal_form_wrapper">
    <form name="unit" method="post" action="" class="skill_modal_form carps_modal_form">
        <input type="hidden" id="security" name="security" value="<?=wp_create_nonce("skill_form");?>" />
        <input type="hidden" id="id" name="id" value="" />
        <div>
            <label for="name">Name</label>
            <input type="text" id="name" name="name" value="" />
        </div>
        <div>
            <label for="description">Description</label>
            <textarea id="description" name="description" rows="10"></textarea>
        </div>
        <div>
            <label for="elements">Elements</label>
            <select class="element_multiple" id="elements" name="elements[]" multiple="multiple">
                <?php
                if (count($displayElements) > 0) {
                    foreach ($displayElements as $element_id => $element_name) {
                        ?>
                <option value="<?=$element_id;?>"><?=$element_name;?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        <hr>
        <div>
            <label>One Shot</label>
            <input class="tgl tgl-flip base_checkbox" type="checkbox" id="is_one_shot" name="is_one_shot" value="" />
            <label class="tgl-btn" data-tg-off="No" data-tg-on="Yes" for="is_one_shot"></label>
        </div>
        <hr>
        <div>
            <label>Type</label>
            <div class="skill_type_container">
                <select class="skill_type" id="skill_type" name="skill_type">
                    <option value=""></option>
                    <?php
                    if (count($skillTypes) > 0) {
                        foreach ($skillTypes as $k => $v) {
                            ?>
                    <option value="<?=$k;?>"><?=$v;?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <hr>
        <div>
            <h3>Add Prerequisite</h3>
            <div class="pre_req_container">
                <label for="pre_req">Add Pre Req Skill</label>
                <select class="pre_req" id="pre_req" name="pre_req">
                    <?php
                    if (count($displaySkills) > 0) {
                        foreach ($displaySkills as $skill_id => $skill_name) {
                            ?>
                    <option value="<?=$skill_id;?>"><?=$skill_name;?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <label for="pre_req_lvl">Level</label>
                <input type="number" id="pre_req_lvl" name="pre_req_lvl" min="0" max="20" value="0" />
                <button class="button" id="add_pre_req" type="button">Add</button>
            </div>
            <hr>
            <div id="hidden_pre_req_list"></div>
            <h3>Prerequisites</h3>
            <ul id="skill_pre_req_list"></ul>
        </div>
        <hr>
        <div>
            <h3>Race (if racial)</h3>
            <div class="races_container">
                <label for="races">Race</label>
                <select class="races" id="races" name="races">
                    <?php
                    if (count($displayRaces) > 0) {
                        foreach ($displayRaces as $race_id => $race_name) {
                            ?>
                    <option value="<?=$race_id;?>"><?=$race_name;?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <hr>
        <div>
            <label>Secret</label>
            <input class="tgl tgl-flip base_checkbox" type="checkbox" id="is_secret_skill" name="is_secret_skill" value="" />
            <label class="tgl-btn" data-tg-off="No" data-tg-on="Yes" for="is_secret_skill"></label>
        </div>
        <hr>
        <div>
            <button class="button">Complete</button>
        </div>
    </form>
</div>
