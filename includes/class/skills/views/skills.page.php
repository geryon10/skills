<?php

/**
 * the unit page
 *
 * @since       0.1
 * @package     project_civitas
 * @subpackage  project_civitas/admin
 */

namespace CarpsCharacterBuilder;

$elements = new Elements(null);
$returnElements = $elements->getDisplayArray();
$displayElements = $returnElements['display'];

$skills = new Skills(null);
$returnSkills = $skills->getDisplayArray();
$displaySkills = $returnSkills['display'];
$arraySkills = $returnSkills['array'];

$races = new Races(null);
$returnRaces = $races->getDisplayArray();
$displayRaces = $returnRaces['display'];

$skillTypes = $skills->skill_types;

ob_start();

add_thickbox();

include 'skills.form.php';

?>
<script>
    var js_skills = [];
    <?php
    if (count($displaySkills) > 0) {
        foreach ($displaySkills as $skill_id => $skill_name) {
            ?>
            js_skills[<?=$skill_id;?>] = '<?=$skill_name;?>';
            <?php
        }
    }
    ?>
</script>
<div id="carps_table_wrapper" class="hide_while_loading">
    <h1>Skills</h1>
    <div class="add_entry_button_wrapper">
        <a href="#TB_inline?width=600&height=550&inlineId=skill_modal" class="thickbox" name="Add New Skill" id="add_new_skill">
            <button class="button">Add New</button>
        </a>
    </div>
    <table id="skill_display_table">
        <thead>
            <tr>
                <th>Edit</th>
                <th>Name</th>
                <th>Element</th>
                <th>Pre Req</th>
                <th>One Shot</th>
                <th>Race</th>
                <th>Secret Skill</th>
                <th>Type</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Edit</th>
                <th>Name</th>
                <th>Element</th>
                <th>Pre Req</th>
                <th>One Shot</th>
                <th>Race</th>
                <th>Secret Skill</th>
                <th>Type</th>
                <th></th>
            </tr>
        </tfoot>
        <tbody>
            <?php
            if (count($arraySkills) > 0) {
                foreach ($arraySkills as $skill) {
                    $nonce = wp_create_nonce("load_skill_" . $skill->getId());
                    $delete_nonce = wp_create_nonce("delete_skill_" . $skill->getId());
                    ?>
                        <tr>
                            <td>
                                <form class="load_skill">
                                    <input type="hidden" id="id" name="id" value="<?=$skill->getId();?>" />
                                    <input type="hidden" id="security" name="security" value="<?=$nonce;?>" />
                                    <button class="button">Edit</button>
                                </form>
                            </td>
                            <td><?=$skill->getName();?></td>
                            <td>
                                <?php
                                if (count($skill->elements) > 0) {
                                    $skillElements = array();
                                    foreach ($skill->elements as $element_id) {
                                        if (array_key_exists($element_id, $displayElements) == true) {
                                            $skillElements[] = $displayElements[$element_id];
                                        }
                                    }
                                    asort($skillElements);
                                    echo \implode(', ', $skillElements);
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (count($skill->preReqs) > 0) {
                                    $skillPreReqs = array();
                                    foreach ($skill->preReqs as $preReq) {
                                        if (count($preReq) > 0) {
                                            foreach ($preReq as $skill_id => $level) {
                                                if (array_key_exists($skill_id, $displaySkills) == true) {
                                                    $skillPreReq = $displaySkills[$skill_id] . ' lvl ' . $level;
                                                    if (\in_array($skillPreReq, $skillPreReqs) == false) {
                                                        $skillPreReqs[] = $skillPreReq;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    asort($skillPreReqs);
                                    echo \implode(', ', $skillPreReqs);
                                }
                                ?>
                            </td>
                            <td><?=$skill->isOneShot == 1 ? 'Yes' : 'No';?></td>
                            <td>
                                <?php
                                if ($skill->getRaceId() != null && $skill->getRaceId() > 0) {
                                    echo $displayRaces[$skill->getRaceId()];
                                }
                                ?>
                            </td>
                            <td><?=$skill->isSecret == 1 ? 'Yes' : 'No';?></td>
                            <td><?=$skillTypes[$skill->getType()];?></td>
                            <td>
                                <div class="delete_skill" onclick="deleteSkill('<?=$skill->getId();?>', '<?=$delete_nonce;?>', '<?=$skill->getName();?>')">
                                    <input type="hidden" id="id" name="id" value="<?=$skill->getId();?>" />
                                    <input type="hidden" id="security" name="security" value="<?=$delete_nonce;?>" />
                                    <button class="button btn-edit">Delete</button>
                            </div>
                            </td>
                        </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
<?php
echo ob_get_clean();
