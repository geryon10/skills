<?php

/**
 * The admin specific functionality of the plugin
 *
 * @since       1.0
 * @package     carps_character_plugin
 * @subpackage  carps_character_plugin/admin
 */

namespace CarpsCharacterBuilder;

/**
 * All the actual functions to hook into WordPress
 */
class CarpsCharacterAdmin
{
    /**
     * The ID of this plugin.
     *
     * @since   0.1
     * @access  private
     * @var     string $pluginName The ID of this plugin.
     *
     * @return null
     */
    private $pluginName;
    /**
     * The version of this plugin.
     *
     * @since   0.1
     * @access  private
     * @var     string $version The current version of this plugin.
     *
     * @return null
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since   0.1
     * @access  public
     *
     * @param   string $pluginName The name of this plugin.
     * @param   string $version The version of this plugin.
     *
     * @return null
     */
    public function __construct(string $pluginName, string $version)
    {
        $this->plugin_name = $pluginName;
        $this->version = $version;
    }

    public function createActions()
    {
        //  add the menu items
        add_action(
            'admin_menu',
            [$this, 'registerMenu',]
        );

        // Enqueue the front end js and styles
        add_action(
            'wp_enqueue_scripts',
            [$this, 'loadDatatables',]
        );
        add_action(
            'wp_enqueue_scripts',
            [$this, 'loadFrontJs',]
        );
        add_action(
            'wp_enqueue_scripts',
            [$this, 'loadFrontCss',]
        );

        //  add the stylesheets
        add_action(
            'admin_enqueue_scripts',
            [$this, 'loadAdminStylesheet',]
        );
        add_action(
            'admin_enqueue_scripts',
            [$this, 'loadDatatables',]
        );
        add_action(
            'admin_enqueue_scripts',
            [$this, 'loadSelectTwo',]
        );
        add_action(
            'admin_enqueue_scripts',
            [$this, 'loadAdminJquery',]
        );

        //  get the item from DB ajax
        add_action(
            'wp_ajax_load_element',
            [$this, 'ajaxLoadElement']
        );
        add_action(
            'wp_ajax_load_skill',
            [$this, 'ajaxLoadSkill']
        );
        add_action(
            'wp_ajax_load_race',
            [$this, 'ajaxLoadRaces']
        );

        //  add the ajax add/update
        add_action(
            'wp_ajax_add_edit_element',
            [$this, 'addEditElement']
        );
        add_action(
            'wp_ajax_add_edit_skill',
            [$this, 'ajaxAddEditSkill']
        );
        add_action(
            'wp_ajax_add_edit_race',
            [$this, 'ajaxAddEditRace']
        );

        add_action(
            'wp_ajax_delete_skill',
            [$this, 'ajaxDeleteSkill']
        );
        add_action(
            'wp_ajax_delete_element',
            [$this, 'ajaxDeleteElement']
        );
        add_action(
            'wp_ajax_delete_race',
            [$this, 'ajaxDeleteRace']
        );
    }

    /**
     * Enqueue the Datatable css and js
     * this function is called from builder.class.php::defineAdminHooks
     *
     * @since   0.1
     * @access  public
     */
    public function loadDatatables()
    {
        wp_register_script(
            'jquery_datatables_js',
            'https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js',
            [],
            null,
            true
        );
        wp_enqueue_script('jquery_datatables_js');

        wp_register_style(
            'jquery_datatables_css',
            'https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css'
        );
        wp_enqueue_style('jquery_datatables_css');

        /*
        Registers the JS variable of ajaxURL to be used with datatables
        Change the nonce name and nonce string to the plugin name
        */
        $localizeScript = [
            'ajaxURL' => [
                'ajaxURL' => admin_url('admin-ajax.php'),
            ],
            'tableNonce' => wp_create_nonce('carps_character-table-nonce'),
        ];
        wp_localize_script('carps_character_data_tables', 'carps_character_data_tables', $localizeScript);
    }

    /**
     * enqueue select2 jquery
     *
     * @since   0.1
     * @access  public
     */
    public function loadSelectTwo()
    {
        wp_register_style(
            'select2_css',
            'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css'
        );
        wp_enqueue_style('select2_css');

        wp_register_script(
            'select2_js',
            'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js',
            array('jquery')
        );
        wp_enqueue_script('select2_js');
    }

    /**
     * enqueue main jquery
     *
     * @since   0.1
     * @access  public
     */
    public function loadAdminJquery()
    {
        wp_register_script('jquery_admin_main', plugins_url('js/main.admin.js', CARPS_CHARACTER_FILE), array('jquery'));
        wp_enqueue_script('jquery_admin_main');

        //  admin unit JS
        wp_register_script(
            'jquery_admin_element',
            plugins_url('js/elements.admin.js', CARPS_CHARACTER_FILE),
            array('jquery')
        );
        wp_enqueue_script('jquery_admin_element');
        wp_register_script(
            'jquery_admin_skill',
            plugins_url('js/skills.admin.js', CARPS_CHARACTER_FILE),
            array('jquery')
        );
        wp_enqueue_script('jquery_admin_skill');
        wp_register_script(
            'jquery_admin_race',
            plugins_url('js/races.admin.js', CARPS_CHARACTER_FILE),
            array('jquery')
        );
        wp_enqueue_script('jquery_admin_race');
    }

    /**
     * Enqueue the js file to be used on the front end
     *
     * @since  0.1
     * @access public
     */
    public function loadFrontJs()
    {
        wp_register_script(
            'carps_character_js',
            plugins_url('js/carps.js', CARPS_CHARACTER_FILE),
            [ 'jquery', 'jquery-ui-core', 'jquery-ui-sortable' ]
        );
        wp_enqueue_script('carps_character_js');
    }

    /**
     * Enqueue the css file to be used on the front end
     *
     * @since  0.1
     * @access public
     */
    public function loadFrontCss()
    {
        wp_register_style(
            'carps_character_css',
            plugins_url('css/styles.css', CARPS_CHARACTER_FILE)
        );
        wp_enqueue_style('carps_character_css');
    }

    public function registerMenu()
    {
        // loads the carps menu and sub menus
        add_menu_page(
            "Carps",
            'Carps',
            "carps_contributor",
            "carps",
            array( $this, "displayCarpsBasePage")
        );
        add_submenu_page(
            "carps",
            __("Elements", $this->plugin_name),
            __("Elements", $this->plugin_name),
            'carps_contributor',
            'elements',
            array( $this, 'displayElements')
        );
        add_submenu_page(
            "carps",
            __("Skills", $this->plugin_name),
            __("Skills", $this->plugin_name),
            'carps_contributor',
            'skills',
            array( $this, 'displaySkills')
        );
        add_submenu_page(
            "carps",
            __("Races", $this->plugin_name),
            __("Races", $this->plugin_name),
            'carps_contributor',
            'races',
            array( $this, 'displayRaces')
        );
    }

    /**
     * enqueue the admin CSS
     *
     * @since 1.0
     */
    public function loadAdminStylesheet()
    {
        wp_register_style('admin_css', plugins_url('css/admin_styles.css', CARPS_CHARACTER_FILE));
        wp_enqueue_style('admin_css');
    }

    /**
     * what page will be loaded when select from the admin menu
     *
     * @since   1.0
     * @access  public
     */
    public function displayCarpsBasePage()
    {
        //  include_once CARPS_CHARACTER_FOLDER . "/views/unit.page.php";
        \ob_start();
        ?>
        <h1>Carps</h1>
        <?php
        echo \ob_get_clean();
    }

    public function displayElements()
    {
        include_once CARPS_CHARACTER_CLASS_FOLDER . DIRECTORY_SEPARATOR . 'elements' . DIRECTORY_SEPARATOR . 'views'
            . DIRECTORY_SEPARATOR . 'elements.page.php';
    }

    public function displaySkills()
    {
        include_once CARPS_CHARACTER_CLASS_FOLDER . DIRECTORY_SEPARATOR . 'skills' . DIRECTORY_SEPARATOR . 'views'
            . DIRECTORY_SEPARATOR . 'skills.page.php';
    }

    public function displayRaces()
    {
        include_once CARPS_CHARACTER_CLASS_FOLDER . DIRECTORY_SEPARATOR . 'races' . DIRECTORY_SEPARATOR . 'views'
            . DIRECTORY_SEPARATOR . 'races.page.php';
    }

    //  ----------  //
    /**
     * the database install function
     * generates the SQL and table name for the install tables function to actually install
     *
     * @since   0.1
     * @access  private
     */
    public function installCarpsSkills()
    {
        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();

        $elements = array(
            'table_name' => 'carps_elements',
            'sql' => "CREATE TABLE `carps_elements` (
                `id` int(12) NOT NULL AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                PRIMARY KEY  (id)
                )$charset_collate;"
        );
        $this->pluginInstallTables($elements);

        $races = array(
            'table_name' => 'carps_races',
            'sql' => "CREATE TABLE `carps_races` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                `description` text NOT NULL,
                `is_secret` tinyint(1) NOT NULL DEFAULT '0',
                PRIMARY KEY  (id)
                )$charset_collate;"
        );
        $this->pluginInstallTables($races);

        $skills = array(
            'table_name' => 'carps_skills',
            'sql' => "CREATE TABLE `carps_skills` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                `description` text NOT NULL,
                `is_one_shot` tinyint(1) NOT NULL DEFAULT '0',
                `race_id` int(12) DEFAULT NULL,
                `is_secret` tinyint(1) NOT NULL DEFAULT '0',
                `type` varchar(2) DEFAULT NULL,
                PRIMARY KEY  (id)
                )$charset_collate;"
        );
        $this->pluginInstallTables($skills);

        $skillsElementBridge = array(
            'table_name' => 'carps_skill_element_bridge',
            'sql' => "CREATE TABLE `carps_skill_element_bridge` (
                `element_id` int(12) NOT NULL,
                `skill_id` int(12) NOT NULL
                )$charset_collate;"
        );
        $this->pluginInstallTables($skillsElementBridge);

        $skillsPreReqBridge = array(
            'table_name' => 'carps_skill_pre_req_bridge',
            'sql' => "CREATE TABLE `carps_skill_pre_req_bridge` (
                `skill_id` int(12) NOT NULL,
                `pre_req_skill_id` int(12) NOT NULL,
                `level_required` int(11) NOT NULL,
                PRIMARY KEY  (id)
                )$charset_collate;"
        );
        $this->pluginInstallTables($skillsPreReqBridge);
    }
    /**
     * the actual function that creates the tables
     *
     * @since   0.1
     * @access  private
     *
     * @param   string      $table  the SQL string to create the table
     */
    private function pluginInstallTables($table)
    {
        global $wpdb;

        //  only run the creation script if the table does not exist
        if ($wpdb->get_var("show tables like '" . $table['table_name'] . "'") != $table['table_name']) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($table['sql']);
        }
    }

    //  ----------  //
    //  AJAX calls  //
    //  ----------  //
    /**
     * the ajax function that returns a Unit
     *
     * @since 01.0
     */
    public function ajaxLoadElement()
    {
        $element = new Elements();
        $element->ajaxLoad();
    }
    public function addEditElement()
    {
        $element = new Elements();
        $element->addEditElement();
    }
    public function ajaxDeleteElement()
    {
        $element = new Elements();
        $element->ajaxDeleteElement();
    }

    public function ajaxLoadSkill()
    {
        $skills = new Skills();
        $skills->ajaxLoad();
    }
    public function ajaxAddEditSkill()
    {
        $skills = new Skills();
        $skills->ajaxAddEditSkill();
    }
    public function ajaxDeleteSkill()
    {
        $skills = new Skills();
        $skills->ajaxDeleteSkill();
    }

    public function ajaxLoadRaces()
    {
        $races = new Races();
        $races->ajaxLoad();
    }
    public function ajaxAddEditRace()
    {
        $races = new Races();
        $races->ajaxAddEditRace();
    }
    public function ajaxDeleteRace()
    {
        $races = new Races();
        $races->ajaxDeleteRace();
    }
}
