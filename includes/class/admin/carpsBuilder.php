<?php

/**
 * The builder class
 *
 * @since       1.0
 * @package     carps_character_plugin
 * @subpackage  carps_character_plugin/admin
 */

namespace CarpsCharacterBuilder;

/**
 * Sets the class for building the WordPress plugin
 */
class CarpsCharacterBuilder
{
    /**
     * The unique identifier of this plugin.
     *
     * @since    0.1
     * @access   protected
     * @var      string $pluginName The string used to uniquely identify this plugin.
     */
    protected $pluginName;
    /**
     * The current version of the plugin.
     *
     * @since    0.1
     * @access   protected
     * @var      string $version The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the
     * plugin. Load the dependencies, define the locale, and set the hooks for
     * the admin area and the public-facing side of the site.
     *
     * @since    0.1
     * @access   public
     */
    public function __construct()
    {
        $this->plugin_name = 'CARPS Character';
        $this->version = '1';

        $this->loadDependencies();
        $this->defineAdminHooks();
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - InpsydeUsersLoader. Orchestrates the hooks of the plugin.
     *
     * Create an instance of the loader which will be used to register the hooks with WordPress.
     *
     * @since    0.1
     * @access   public
     */
    public function loadDependencies()
    {
        // The base admin classes
        require_once CARPS_CHARACTER_FOLDER . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'carpsAdmin.php';
        require_once CARPS_CHARACTER_FOLDER . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'base.class.php';
        require_once CARPS_CHARACTER_FOLDER . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'elements' . DIRECTORY_SEPARATOR . 'elements.class.php';
        require_once CARPS_CHARACTER_FOLDER . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'skills' . DIRECTORY_SEPARATOR . 'skills.class.php';
        require_once CARPS_CHARACTER_FOLDER . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'races' . DIRECTORY_SEPARATOR . 'races.class.php';

        require_once CARPS_CHARACTER_FOLDER . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'shortcode' . DIRECTORY_SEPARATOR . 'races.shortcode.php';
        require_once CARPS_CHARACTER_FOLDER . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'shortcode' . DIRECTORY_SEPARATOR . 'skills.shortcode.php';
    }

    /**
     * Register all of the hooks related to the admin area functionality of the plugin.
     * the default is from the CarpsCharacterAdmin class but can be changed
     * uses the built in WP functions
     * admin_menu - for adding to the admin menu
     * admin_enqueue_scripts - for enqueuing both JS and CSS to the admin pages
     * wp_enqueue_scripts - for enqueuing both JS and CSS to front facing pages
     * wp_ajax_ - for ajax calls
     *
     * @since    0.1
     * @access   public
     */
    public function defineAdminHooks()
    {
        $pluginAdmin = new CarpsCharacterAdmin($this->pluginName(), $this->version());

        $pluginAdmin->createActions();
        $pluginAdmin->installCarpsSkills();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since   0.1
     * @access  public
     *
     * @return  string  The name of the plugin.
     */
    public function pluginName(): string
    {
        return $this->plugin_name;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since   0.1
     * @access  public
     *
     * @return  string  The version number of the plugin.
     */
    public function version(): string
    {
        return $this->version;
    }
}
