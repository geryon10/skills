<?php

/**
 * the base class that contains the functions for the variables id, created_at,
 * updated_at, base_query and tableAs.
 * Contains the functions get_all_from_db,get_from_db_by_id, generate_object,
 * insert_update, delete and ajax_load
 *
 * @since       1.0
 * @package     carps_character_plugin
 * @subpackage  carps_character_plugin/admin
 */

namespace CarpsCharacterBuilder;

class CarpsBaseClass
{
    //  ------------------------------------------------------- //
    //      Variables for most of the derived child classes     //
    //  ------------------------------------------------------- //
    /**
     * id of the note link
     *
     * @since   0.1
     * @access  public
     * @var     int     $id
     */
    public $id;
    /**
     * date row was created
     *
     * @since   0.1
     * @access  public
     * @var     string  $created_at
     */
    public $created_at;
    /**
     * date row was updated
     *
     * @since   0.1
     * @access  public
     * @var     string  $updated_at
     */
    public $updated_at;
    /**
     * the base query string for getting the information from the DB
     *
     * @since   0.1
     * @access  public
     * @var     string $base_query
     */
    public $base_query;
    /**
     * name of the table
     *
     * @since   0.1
     * @access  public
     * @var     string $base_query
     */
    public $tableAs;
    /**
     * name of the DB plugin subname
     *
     * @since   0.1
     * @access  public
     * @var     string $db
     */
    public $db = 'carps_';

    //  ------------------------------------------  //
    //      the basic set variable functions        //
    //  ------------------------------------------  //
    /**
     * sets the class variable id
     *
     * @since   0.1
     * @access  public
     * @param   int     $id the id of the row
     *
     * @return  null
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * sets the class variable created_at
     *
     * @since   0.1
     * @access  public
     * @param   string  $created_at date the make was created
     *
     * @return  null
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }
    /**
     * sets the class variable updated_at
     *
     * @since   0.1
     * @access  public
     * @param   string  $updated_at date the make was updated
     *
     * @return  null
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }
    /**
     * sets the class variable tableAs
     *
     * @since   0.1
     * @access  public
     * @param   string  $tableAs what the table should be called
     *
     * @return  null
     */
    public function setTableAs($tableAs)
    {
        $this->tableAs = $tableAs;
    }

    //  ----------------------------------  //
    //      the get variable functions      //
    //  ----------------------------------  //
    /**
     * retrieves the class variable id
     *
     * @since   0.1
     * @access  public
     *
     * @return  int this->id
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * retrieves the class variable created_at
     *
     * @since   0.1
     * @access  public
     *
     * @return  string  this->created_at
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    /**
     * retrieves the class variable updated_at
     *
     * @since   0.1
     * @access  public
     *
     * @return  string  this->updated_at
     */
    public function getUpdatedAt(): string
    {
        return $this->updated_at;
    }

    public function getTableAs()
    {
        return $this->tableAs;
    }

    //  --------------------------------------------------  //
    //      the functions that get the rows from the DB     //
    //  --------------------------------------------------  //
    /**
     * retrieves all rows from the DV
     *
     * @since 0.1
     * @access public
     *
     * @return array $array returns the array of objects
     */
    public function getAllFromDb($where = null): array
    {
        global $wpdb;

        $query = $this->base_query;

        if ($where != null) {
            $query .= ' ' . $where;
        }
        $results = $wpdb->get_results($query);
        return $this->generateObject($results);
    }
    /**
     * retrieves the row from the db by the passed id
     *
     * @since 0.1
     * @access public
     *
     * @return array $array returns an array like all query functions
     */
    public function getFromDbById($id): array
    {
        global $wpdb;

        $query = "{$this->base_query}
        WHERE `{$this->tableAs}`.`id` = %d
        ";
        $results = $wpdb->get_results($wpdb->prepare($query, $id));
        return $this->generateObject($results);
    }

    /**
     * will generate an array of the objects
     *
     * @since 0.1
     * @access private
     * @param object $results the WP result from the query
     *
     * @return array $array array of the objects
     */
    public function generateObject($results): array
    {
        $class = get_class($this);    //  what class we are dealing with
        if (count($results) > 0 && $class != '') {
            foreach ($results as $result) {
                $array[] = new $class($result);
            }
        } else {
            $array = array();
        }
        return $array;
    }

    /**
     * either adds a new product_price or edits an existing one
     *
     * @since   0.1
     * @access  public
     * @param   object          $array            column names and values to insert/update
     * @param   bool   optional $dates            if the table has created_at and updated_at columns
     * @param   bool   optional $echo             to echo a response
     *
     * @return int $id returns the id for associating other tables
     */
    public function insertUpdate($array, $dates = true, $echo = true): int
    {
        global $wpdb;
        if ($dates) {
            $updated_at = date('Y-m-d H:i:s');   //  only adding dates on tables that need it
        }

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if ($k != 'id' && $k != 'add_edit') {
                    $arrayNamesValues[$k] = $v;   //  we don't need the id or the add or edit variable
                    $arrayType[] = '%s';
                }
            }
            //  only add updated and created on tables that have them
            if ($dates) {
                $arrayNamesValues['updated_at'] = $updated_at;
                $arrayType[] = '%s';
                if ($array->add_edit == 'add') {
                    $arrayNamesValues['created_at'] = $updated_at; //  only add created if it is a new entry
                    $arrayType[] = '%s';
                }
            }
        }

        // the actual WP insert/update
        if ($array->add_edit === 'add') {
            $wpdb->insert(
                $wpdb->prefix . $this->tableAs,
                $arrayNamesValues,
                $arrayType
            );
            $id = $wpdb->insert_id;
            if ($echo == true) {
                echo json_encode(
                    array(
                        'id'            => $id,
                        'action'        => 'added',
                        'load_nounce'   => wp_create_nonce('load_' . $this->tableAs . '_' . $id),
                        'delete_nounce' => wp_create_nonce('delete_' . $this->tableAs . '_' . $id)
                    )
                );
            }
        } elseif ($array->add_edit === 'edit') {
            $wpdb->update(
                $wpdb->prefix . $this->tableAs,
                $arrayNamesValues,
                array( 'id' => $array->id ),
                $arrayType,
                array( '%d' )
            );
            if ($echo == true) {
                echo json_encode(
                    array(
                        'id'            => $id,
                        'action'        => 'updated',
                        'load_nounce'   => wp_create_nonce('load_' . $this->tableAs . '_' . $id),
                        'delete_nounce' => wp_create_nonce('delete_' . $this->tableAs . '_' . $id)
                    )
                );
            }
            $id = $array->id;
        }
        return $id;
    }

    /**
     * removes the selected row from the DB
     *
     * @since 0.1
     * @access private
     * @param int $id what row to delete
     *
     * @return null
     */
    public function delete($id)
    {
        global $wpdb;

        $wpdb->delete(
            $wpdb->prefix . $this->tableAs,
            array('id' => $id),
            array('%d')
        );
    }

    /**
     * called from ajax
     * will retrieve the row from the DB from the post id field
     *
     * @since 0.1
     * @access public
     *
     * @return null
     */
    public function ajaxLoad()
    {
        global $wpdb;
        $id = sanitize_text_field($_POST['id']);

        if ($id > 0) {
            $array = $this->getFromDbById($id);
            //  all get db calls return arrays
            if (count($array) > 0) {
                $output = $array[0]->createObject();
            }
        } else {
            $output = 'Something went wrong.';
        }
        echo json_encode($output);

        wp_die();   //  needed on all WP ajax functions
    }

    public function getDisplayArray(): array
    {
        $array = $this->getAllFromDb();
        $displayArray = array();
        if (count($array) > 0) {
            foreach ($array as $item) {
                $displayArray[$item->getId()] = $item->getName();
            }
            asort($displayArray);
        }
        return array('display' => $displayArray, 'array' => $array);
    }
}
