<div id="element" class="carps_modal_form_wrapper">
    <form name="unit" method="post" action="" class="element_modal_form carps_modal_form">
        <input type="hidden" id="security" name="security" value="<?=wp_create_nonce("element_form");?>" />
        <input type="hidden" id="id" name="id" value="" />
        <div>
            <label for="name">Name</label>
            <input type="text" id="name" name="name" value="" />
        </div>
        <div>
            <button class="button">Complete</button>
        </div>
    </form>
</div>