<?php

/**
 * the unit page
 *
 * @since       0.1
 * @package     project_civitas
 * @subpackage  project_civitas/admin
 */

namespace CarpsCharacterBuilder;

$elements = new Elements(null);
$arrayElements = $elements->getAllFromDb();

ob_start();

add_thickbox();

include 'elements.form.php';

?>
<div id="carps_table_wrapper" class="hide_while_loading">
    <h1>Units</h1>
    <div class="add_entry_button_wrapper">
        <a href="#TB_inline?width=600&height=550&inlineId=element" class="thickbox" name="Add New" id="">
            <button class="button">Add New</button>
        </a>
    </div>
    <table id="element_display_table">
        <thead>
            <tr>
                <th>Edit</th>
                <th>Name</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Edit</th>
                <th>Name</th>
                <th></th>
            </tr>
        </tfoot>
        <tbody>
            <?php
            if (count($arrayElements) > 0) {
                foreach ($arrayElements as $element) {
                    ?>
                        <tr>
                            <td>
                                <form class="load_element">
                                    <input type="hidden" id="id" name="id" value="<?=$element->getId();?>" />
                                    <input type="hidden" id="security" name="security" value="<?=wp_create_nonce("load_element_" . $element->getId());?>" />
                                    <button class="button">Edit</button>
                                </form>
                            </td>
                            <td><?php echo $element->getName();?></td>
                            <td>
                                <form class="delete_element">
                                    <input type="hidden" id="id" name="id" value="<?=$element->getId();?>" />
                                    <input type="hidden" id="security" name="security" value="<?=$delete_nonce;?>" />
                                    <button class="button btn-edit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
<?php
echo ob_get_clean();
