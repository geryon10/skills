<?php

namespace CarpsCharacterBuilder;

class Elements extends CarpsBaseClass
{
    public function __construct($result = array())
    {
        if (isset($result->id)) {
            $this->setId($result->id);
        }
        if (isset($result->name)) {
            $this->setName($result->name);
        }
        $this->setTableAs($this->db . 'elements');
        $this->base_query = "SELECT `{$this->tableAs}`.`id`,
        `{$this->tableAs}`.`name`
        FROM `{$this->tableAs}` AS `{$this->tableAs}`
        ";
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function createObject()
    {
        $object = new \stdClass();
        $object->id = $this->getId();
        $object->name = $this->getName();

        return $object;
    }

    public function addEditElement()
    {
        global $wpdb;

        check_ajax_referer('element_form', 'security');

        $id = $_POST['id'];
        $add_edit = sanitize_text_field($_POST['add_edit']);
        $name = sanitize_text_field($_POST['name']);

        if ($add_edit === 'add') {
            $created = date("Y-m-d H:i:s");
            $wpdb->insert(
                $this->getTableAs(),
                array(
                    'name' => $name,
                ),
                array(
                    '%s',
                )
            );
        } elseif ($add_edit === 'edit') {
            $wpdb->update(
                $this->getTableAs(),
                array(
                    'name' => $name,
                ),
                array( 'id' => $id ),
                array(
                    '%s',
                    '%s',
                ),
                array( '%d' )
            );
        } else {
            echo "unknown action";
        }

        wp_die();
    }

    public function ajaxDeleteElement()
    {
        $id = sanitize_text_field($_POST['id']);

        global $wpdb;

        $wpdb->delete(
            'carps_skill_element_bridge',
            array('element_id'   => $id,),
            array('%d',)
        );

        $wpdb->delete(
            $this->tableAs,
            array('id' => $id),
            array('%d')
        );
        wp_die();
    }
}
