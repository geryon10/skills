<?php

namespace CarpsCharacterBuilder;

class Races extends CarpsBaseClass
{
    private $description = '';
    public $isSecret = 0;

    public function __construct($result = array())
    {
        if (isset($result->id)) {
            $this->setId($result->id);
        }
        if (isset($result->name)) {
            $this->setName($result->name);
        }
        if (isset($result->description)) {
            $this->setDescription($result->description);
        }
        if (isset($result->is_secret)) {
            $this->setIsSecret($result->is_secret);
        }
        $this->setTableAs($this->db . 'races');
        $this->base_query = "SELECT `{$this->tableAs}`.`id`,
        `{$this->tableAs}`.`name`,
        `{$this->tableAs}`.`description`,
        `{$this->tableAs}`.`is_secret`
        FROM `{$this->tableAs}` AS `{$this->tableAs}`
        ";
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setIsSecret($is_secret)
    {
        $this->isSecret = $is_secret;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getIsSecret(): int
    {
        return $this->isSecret;
    }

    public function createObject()
    {
        $object = new \stdClass();
        $object->id = $this->getId();
        $object->name = $this->getName();
        $object->description = $this->getDescription();
        $object->isSecret = $this->getIsSecret();

        return $object;
    }

    public function ajaxAddEditRace()
    {
        global $wpdb;

        check_ajax_referer('race_form', 'security');

        $id = $_POST['id'];
        $add_edit = sanitize_text_field($_POST['add_edit']);
        $name = sanitize_text_field($_POST['name']);
        $description = sanitize_textarea_field($_POST['description']);
        $is_secret = sanitize_text_field($_POST['is_secret']);

        if ($add_edit === 'add') {
            $created = date("Y-m-d H:i:s");
            $wpdb->insert(
                $this->getTableAs(),
                array(
                    'name'        => $name,
                    'description' => $description,
                    'is_secret'   => $is_secret,
                ),
                array(
                    '%s',
                    '%s',
                    '%s',
                )
            );
        } elseif ($add_edit === 'edit') {
            $wpdb->update(
                $this->getTableAs(),
                array(
                    'name'        => $name,
                    'description' => $description,
                    'is_secret'   => $is_secret,
                ),
                array( 'id' => $id ),
                array(
                    '%s',
                    '%s',
                    '%s',
                ),
                array( '%d' )
            );
        } else {
            echo "unknown action";
        }

        wp_die();
    }

    public function ajaxDeleteRace()
    {
        $id = sanitize_text_field($_POST['id']);

        global $wpdb;

        $wpdb->delete(
            $this->tableAs,
            array('id' => $id),
            array('%d')
        );
        wp_die();
    }

    /**
     * to get the races and their skills in a nice organized array
     *
     * @return array
     */
    public function getRacesAndRacialSkill(): array
    {
        global $wpdb;

        $races = array();

        $skills = new Skills(null);

        //  just the one query
        $query = "SELECT `{$this->tableAs}`.`id` AS `race_id`,
        `{$this->tableAs}`.`name` AS `race_name`,
        `{$this->tableAs}`.`description` AS `race_description`,
        `{$this->tableAs}`.`is_secret` AS `race_is_secret`,
        `{$skills->tableAs}`.`id` AS `skill_id`,
        `{$skills->tableAs}`.`name` AS `skill_name`,
        `{$skills->tableAs}`.`description` AS `skill_description`,
        `{$skills->tableAs}`.`is_one_shot`,
        `{$skills->tableAs}`.`is_secret`,
        `{$skills->tableAs}`.`type`,
        `{$skills->reqBridge}`.`pre_req_skill_id`
        FROM `{$this->tableAs}` AS `{$this->tableAs}`
        LEFT JOIN `{$skills->tableAs}` AS `{$skills->tableAs}` ON `{$this->tableAs}`.`id` = `{$skills->tableAs}`.`race_id`
        LEFT JOIN `{$skills->reqBridgeAs}` AS `{$skills->reqBridgeAs}` ON `{$skills->tableAs}`.`id` = `{$skills->reqBridgeAs}`.`skill_id`
        ORDER BY `race_name` ASC
        ";

        $results = $wpdb->get_results($query);

        if (count($results) > 0) {
            foreach ($results as $r) {
                //  if the race is already in the list add the next skill
                if (array_key_exists($r->race_id, $races) == true) {
                    if ($r->skill_id > 0) {
                        //  if the skill is not in the array or it is does not have prerequisites
                        if (
                            array_key_exists($r->skill_id, $races[$r->race_id]->skills) == true
                            || $r->pre_req_skill_id == 0
                        ) {
                            //  if the skill is already in the array and is not an object make it one
                            if (\is_object($races[$r->race_id]->skills[$r->skill_id]) == false) {
                                $races[$r->race_id]->skills[$r->skill_id] = new \stdClass();
                                $races[$r->race_id]->skills[$r->skill_id]->sub_skills = array();
                            }

                            //  add the details
                            $races[$r->race_id]->skills[$r->skill_id]->name = $r->skill_name;
                            $races[$r->race_id]->skills[$r->skill_id]->description = $r->skill_description;
                            $races[$r->race_id]->skills[$r->skill_id]->is_one_shot = $r->is_one_shot;
                            $races[$r->race_id]->skills[$r->skill_id]->is_secret = $r->is_secret;
                            $races[$r->race_id]->skills[$r->skill_id]->type = $r->type;
                        } else {
                            //  else we need a new skill class with all the details
                            $skill = $this->getRacesAndSkillGenerator($r);
                            $skill_id = $r->pre_req_skill_id;
                            $races[$r->race_id]->skills[$skill_id]->sub_skills[$r->skill_id] = $skill;
                        }
                    }
                } else {
                    //  create the new object to go into the race array
                    $races[$r->race_id] = new \stdClass();
                    $races[$r->race_id]->name = $r->race_name;
                    $races[$r->race_id]->description = $r->race_description;
                    $races[$r->race_id]->is_secret = $r->race_is_secret;
                    $races[$r->race_id]->type = $r->type;
                    $races[$r->race_id]->skills = array();

                    //  if there is an skill
                    if ($r->skill_id > 0) {
                        //  to shorten the skill id and if the skill has a prerequisite
                        if ($r->pre_req_skill_id > 0) {
                            $skill_id = $r->pre_req_skill_id;
                        } else {
                            $skill_id = $r->skill_id;
                        }

                        $races[$r->race_id]->skills[$skill_id] = new \stdClass();
                        $races[$r->race_id]->skills[$skill_id]->sub_skills = array();

                        $skill = $this->getRacesAndSkillGenerator($r);

                        //  if the skill has a prerequisite add the skill to the sub skills of the base skill
                        if ($r->pre_req_skill_id > 0) {
                            $races[$r->race_id]->skills[$skill_id]->sub_skills[$r->skill_id] = $skill;
                        } else {
                            $races[$r->race_id]->skills[$skill_id] = $skill;
                        }
                    }
                }
            }
        }

        return $races;
    }

    public function getRacesAndSkillGenerator($r)
    {
        $skill = new \stdClass();
        $skill->name = $r->skill_name;
        $skill->description = $r->skill_description;
        $skill->is_one_shot = $r->is_one_shot;
        $skill->is_secret = $r->is_secret;
        $skill->type = $r->type;

        return $skill;
    }
}
