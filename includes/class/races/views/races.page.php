<?php

/**
 * the unit page
 *
 * @since       0.1
 * @package     project_civitas
 * @subpackage  project_civitas/admin
 */

namespace CarpsCharacterBuilder;

$races = new Races(null);
$arrayRaces = $races->getAllFromDb();

ob_start();

add_thickbox();

include 'races.form.php';

?>
<div id="carps_table_wrapper" class="hide_while_loading">
    <h1>Units</h1>
    <div class="add_entry_button_wrapper">
        <a href="#TB_inline?width=600&height=550&inlineId=race" class="thickbox" name="Add New" id="add_new_race">
            <button class="button">Add New</button>
        </a>
    </div>
    <table id="race_display_table">
        <thead>
            <tr>
                <th>Edit</th>
                <th>Name</th>
                <th>Secret Race</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Edit</th>
                <th>Name</th>
                <th>Secret Race</th>
                <th></th>
            </tr>
        </tfoot>
        <tbody>
            <?php
            if (count($arrayRaces) > 0) {
                foreach ($arrayRaces as $race) {
                    ?>
                        <tr>
                            <td>
                                <form class="load_race">
                                    <input type="hidden" id="id" name="id" value="<?=$race->getId();?>" />
                                    <input type="hidden" id="security" name="security" value="<?=wp_create_nonce("load_race_" . $race->getId());?>" />
                                    <button class="button">Edit</button>
                                </form>
                            </td>
                            <td><?=$race->getName();?></td>
                            <td><?=$race->isSecret == 1 ? 'Yes' : 'No';?></td>
                            <td>
                                <form class="delete_race">
                                    <input type="hidden" id="id" name="id" value="<?=$race->getId();?>" />
                                    <input type="hidden" id="security" name="security" value="<?=$delete_nonce;?>" />
                                    <button class="button btn-edit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
<?php
echo ob_get_clean();
