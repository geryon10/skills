<div id="race" class="carps_modal_form_wrapper">
    <form name="unit" method="post" action="" class="race_modal_form carps_modal_form">
        <input type="hidden" id="security" name="security" value="<?=wp_create_nonce("race_form");?>" />
        <input type="hidden" id="id" name="id" value="" />
        <div>
            <label for="name">Name</label>
            <input type="text" id="name" name="name" value="" />
        </div>
        <div>
            <label for="description">Description</label>
            <textarea id="description" name="description" rows="10"></textarea>
        </div>
        <hr>
        <div>
            <label>Secret</label>
            <input class="tgl tgl-flip base_checkbox" type="checkbox" id="is_secret_race" name="is_secret_race" value="" />
            <label class="tgl-btn" data-tg-off="No" data-tg-on="Yes" for="is_secret_race"></label>
        </div>
        <div>
            <button class="button">Complete</button>
        </div>
    </form>
</div>