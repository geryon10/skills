<?php

/**
 * adds the hook for the short code and the short code
 *
 * @since       0.1
 * @package     monster_lookup_plugin
 */
function carps_races_shortcode($atts)
{
    //  get all the races and racial skills
    $races = new CarpsCharacterBuilder\Races(null);
    $races = $races->getRacesAndRacialSkill();

    ob_start();
    ?>
    <h2>Races</h2>
    <div class="races_container">
        <ul class="accordion carps_race_accordion_list">
    <?php
    if (count($races) > 0) {
        foreach ($races as $race) {
            ?>
            <li>
                <a class="toggle race_toggle" href="javascript:void(0);"><?=$race->name;?></a>
                <ul class="inner carps_race_accordion_list">
                    <li>
                    <a href="#" class="toggle skill_toggle">Description</a>
                        <ul class="inner carps_race_accordion_list">
                            <li>
                                <p><?=nl2br(stripslashes_deep($race->description));?></p>
                            </li>
                        </ul>
                    </li>
                    <?php
                    if (count($race->skills) > 0) {
                        foreach ($race->skills as $skill) {
                            ?>
                    <li>
                        <a href="#" class="toggle skill_toggle"><?=$skill->name;?> <?=$skill->type != '' ? '(' . $skill->type . ')' : '';?></a>
                        <ul class="inner carps_race_accordion_list">
                            <li>
                                <p><?=nl2br(stripslashes_deep($skill->description));?></p>
                            </li>
                            <?php
                            if (count($skill->sub_skills) > 0) {
                                foreach ($skill->sub_skills as $sub_skill) {
                                    ?>
                            <li>
                                <a href="#" class="toggle sub_skill_toggle"><?=$sub_skill->name;?> <?=$sub_skill->type != '' ? '(' . $sub_skill->type . ')' : '';?></a>
                                <div class="inner">
                                    <p><?=nl2br(stripslashes_deep($sub_skill->description));?></p>
                                </div>
                            </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </li>
            <?php
        }
    }
    ?>
        </ul>
    </div>
    <script>
        jQuery(document).ready(function (jQuery) {
            jQuery('.toggle').click(function(e) {
                e.preventDefault();

                var $this = jQuery(this);

                if ($this.next().hasClass('show')) {
                    $this.next().removeClass('show');
                    $this.next().slideUp(350);
                } else {
                    $this.parent().parent().find('div .inner').removeClass('show');
                    $this.parent().parent().find('div .inner').slideUp(350);
                    $this.next().toggleClass('show');
                    $this.next().slideToggle(350);
                }
            });
        });
    </script>
    <?php
    return ob_get_clean();
}
add_shortcode('carps_races_shortcode', 'carps_races_shortcode');
