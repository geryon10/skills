<?php

function carps_skills_shortcode($atts)
{
    $elements = new CarpsCharacterBuilder\Elements(null);
    $returnElements = $elements->getDisplayArray();
    $displayElements = $returnElements['display'];

    $races = new CarpsCharacterBuilder\Races(null);
    $returnRaces = $races->getDisplayArray();
    $displayRaces = $returnRaces['display'];

    $skills = new CarpsCharacterBuilder\Skills(null);
    $returnSkills = $skills->getDisplayArray();
    $displaySkills = $returnSkills['display'];
    $arraySkills = $returnSkills['array'];

    $skillTypes = $skills->skill_types;

    ob_start();

    add_thickbox();

    if (count($arraySkills) > 0) {
        foreach ($arraySkills as $skill) {
            ?>
            <div id="skill_modal_<?=$skill->getId();?>" class="carps_skill_modal">
                <?php
                if (count($skill->preReqs) > 0) {
                    ?>
                <p>
                    <strong>Prerequisites</strong>
                    <br>
                    <?php
                    $skillPreReqs = array();
                    foreach ($skill->preReqs as $preReq) {
                        if (count($preReq) > 0) {
                            foreach ($preReq as $skill_id => $level) {
                                if (array_key_exists($skill_id, $displaySkills) == true) {
                                    $temp_skill_id = "'" . $skill_id . "'";
                                    $temp_name = "'" . $displaySkills[$skill_id] . "'";
                                    $skillPreReq = '<a onclick="loadDifSkill(' . $temp_skill_id . ', ' . $temp_name . ')"> lvl ' . $level . ' <strong>' . $displaySkills[$skill_id] . '</strong></a>';
                                    if (\in_array($skillPreReq, $skillPreReqs) == false) {
                                        $skillPreReqs[] = $skillPreReq;
                                    }
                                }
                            }
                        }
                    }
                    asort($skillPreReqs);

                    $preReqOutput = implode(', ', $skillPreReqs);

                    echo $preReqOutput;
                    ?>
                </p>
                <hr>
                    <?php
                }
                ?>
                <p><?=nl2br(stripslashes_deep($skill->description));?></p>
                <?php
                if (count($skill->oneShots) > 0) {
                    ?>
                    <hr>
                    <p>
                        <strong>One Shots</strong>
                        <br>
                        <?php
                        foreach ($skill->oneShots as $oneShot) {
                            if (count($oneShot) > 0) {
                                foreach ($oneShot as $skill_id => $level) {
                                    ?>
                        <div>
                            <a onclick="loadDifSkill('<?=$skill_id;?>', '<?=$displaySkills[$skill_id];?>')">
                                lvl <?=$level;?> <strong><?=$displaySkills[$skill_id];?></strong>
                            </a>
                        </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </p>
                    <?php
                }
                ?>
            </div>
            <?php
        }
    }

    ?>
    <div id="carps_table_wrapper" class="hide_while_loading">
        <table id="skill_display_table" class="display responsive nowrap" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Element</th>
                    <!-- <th>Pre Req</th>
                    <th>One Shot</th>
                    <th>Race</th>
                    <th>Type</th> -->
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Element</th>
                    <!-- <th>Pre Req</th>
                    <th>One Shot</th>
                    <th>Race</th>
                    <th>Type</th> -->
                </tr>
            </tfoot>
            <tbody>
            <?php
            if (count($arraySkills) > 0) {
                foreach ($arraySkills as $skill) {
                    ?>
                <tr>
                    <td>
                        <a href="#TB_inline?width=600&height=550&inlineId=skill_modal_<?=$skill->getId();?>" class="thickbox load_carps_skill" name="<?=$skill->getName();?>" id="view_<?=$skill->getId();?>">
                            <?=$skill->getName();?>
                        </a>
                    </td>
                    <td>
                        <?php
                        if (count($skill->elements) > 0) {
                            $skillElements = array();
                            foreach ($skill->elements as $element_id) {
                                if (array_key_exists($element_id, $displayElements) == true) {
                                    $skillElements[] = $displayElements[$element_id];
                                }
                            }
                            asort($skillElements);
                            echo \implode(', ', $skillElements);
                        }
                        ?>
                    </td>
                    <!-- <td>
                        <?php
                        if (count($skill->preReqs) > 0) {
                            $skillPreReqs = array();
                            foreach ($skill->preReqs as $preReq) {
                                if (count($preReq) > 0) {
                                    foreach ($preReq as $skill_id => $level) {
                                        if (array_key_exists($skill_id, $displaySkills) == true) {
                                            $skillPreReq = $displaySkills[$skill_id] . ' lvl ' . $level;
                                            if (\in_array($skillPreReq, $skillPreReqs) == false) {
                                                $skillPreReqs[] = $skillPreReq;
                                            }
                                        }
                                    }
                                }
                            }
                            asort($skillPreReqs);
                            echo \implode(', ', $skillPreReqs);
                        }
                        ?>
                    </td> -->
                    <!-- <td><?=$skill->isOneShot == 1 ? 'Yes' : 'No';?></td>
                    <td>
                        <?php
                        if ($skill->getRaceId() != null && $skill->getRaceId() > 0) {
                            echo $displayRaces[$skill->getRaceId()];
                        }
                        ?>
                    </td> -->
                    <!-- <td><?=$skillTypes[$skill->getType()];?></td> -->
                </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <script>
        jQuery(document).ready(function (jQuery) {
            jQuery("#skill_display_table").DataTable({
                "order": [[0, "asc"]],
                responsive: true
            });
            jQuery("#skill_display_table").on('click', '.load_carps_skill', function() {
                setTimeout(function() {
                    resizeCarpsSkillModal()
                }, 250);
            });
        });
        function loadDifSkill(id, name)
        {
            tb_remove();

            var url = "#TB_inline?width=600&height=550&inlineId=skill_modal_" + id;

            setTimeout(function() {
                tb_show(name, url);
                resizeCarpsSkillModal()
            }, 250);
        }

        function resizeCarpsSkillModal() {
            var newHeight = jQuery(window).height() - 20;
            var newWidth = jQuery(window).width() - 20;

            if (jQuery(window).width() < 600) {
                jQuery("#TB_window").css("height", (newHeight));
                jQuery("#TB_window").css("width", (newWidth));
                jQuery('#TB_ajaxContent').css("width", (newWidth));
                jQuery('#TB_ajaxContent').css("height", (newHeight));
                jQuery("#TB_window").css("margin-left", 0);
                jQuery("#TB_window").css("margin-top", 0);
                jQuery("#TB_window").css("top", 10);
                jQuery("#TB_window").css("left", 10);
            }
        }
    </script>
    <?php
}
add_shortcode('carps_skills_shortcode', 'carps_skills_shortcode');
